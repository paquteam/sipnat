/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.servicios.base;


import co.edu.ucc.sitnap.modelo.base.Usuario;
import co.edu.ucc.sitnap.seguridad.Md5;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alvaro padilla
 */
@Stateless
public class LogicaLogin {

    @PersistenceContext(unitName = "SITNAPPU")
    private EntityManager em;

    public Usuario loginAdmin(String nombreUsuario, String passwordPlano) {
       try {
            String encPassword = Md5.getEncoddedString(passwordPlano);
            return (Usuario) em.createQuery("Select u from Usuario u where u.nombreUsuario = :n and u.password = :p AND u.tipoUsuario = 'ADMIN'")
                    .setParameter("n", nombreUsuario)
                    .setParameter("p", encPassword).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public Usuario loginCliente(String nombreUsuario, String passwordPlano) {
        try {
            String encPassword = Md5.getEncoddedString(passwordPlano);
            return (Usuario) em.createQuery("Select u from Usuario u where u.nombreUsuario = :n and u.password = :p AND u.tipoUsuario ='CLIENTE'")
                    .setParameter("n", nombreUsuario)
                    .setParameter("p", encPassword).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
