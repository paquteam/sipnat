/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.servicios.base;


import co.edu.ucc.sitnap.modelo.base.CentroPoblado;
import co.edu.ucc.sitnap.modelo.base.Ciudad;
import co.edu.ucc.sitnap.modelo.base.Departamento;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alvaro Padilla
 */
@Stateless
public class LogicaMunicipios {

    @PersistenceContext(unitName = "SITNAPPU")
    private EntityManager em;

    public void limpiarTablas() {
//        System.out.println("Conexion Abierta: "+em.isOpen());
//        em.createNativeQuery("delete from centropoblado").executeUpdate();
//        em.createNativeQuery("delete from ciudad").executeUpdate();
//        em.createNativeQuery("delete from departamento").executeUpdate();
    }

    public void registrarCentroPoblado(String codigoDepartamento, String codigoMunicipio, String codigoCentroPoblado,
            String nombreDepartamento, String nombreMunicipio, String nombreCentroPoblado, String tipo) throws Exception {
        Departamento d;
        try {
            d = (Departamento) em.createQuery("select d from Departamento d where d.codigoDane = :cd")
                    .setParameter("cd", codigoDepartamento).getSingleResult();
        } catch (Exception e) {
            d = new Departamento();
            d.setCodigoDane(codigoDepartamento);
            d.setNombre(nombreDepartamento);
            em.persist(d);
        }
        Ciudad c;
        try {
            c = (Ciudad) em.createQuery("select c from Ciudad c where c.codigoDane = :cd")
                    .setParameter("cd", codigoMunicipio).getSingleResult();
        } catch (Exception e) {
            c = new Ciudad();
            c.setCodigoDane(codigoMunicipio);
            c.setNombre(nombreCentroPoblado);
            c.setDepartamento(d);
            em.persist(c);
        }
        CentroPoblado cp;
        try {
            em.createQuery("select c from CentroPoblado c where c.codigoDane = :cd")
                    .setParameter("cd", codigoDepartamento).getSingleResult();
        } catch (Exception e) {
            cp = new CentroPoblado();
            cp.setCodigoDane(codigoCentroPoblado);
            cp.setNombre(nombreCentroPoblado);
            cp.setCiudad(c);
            em.persist(cp);
        }
    }

    public boolean municipiosCreados() {
        List<CentroPoblado> cs = em.createQuery("select c from CentroPoblado c").setMaxResults(5).getResultList();
        return cs.size()>0;
    }
}
