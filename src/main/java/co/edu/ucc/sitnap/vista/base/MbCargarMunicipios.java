/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.vista.base;



import co.edu.ucc.sitnap.servicios.base.LogicaMunicipios;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.apache.poi.ss.usermodel.Row;
import javax.faces.bean.ViewScoped;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Alvaro Padilla
 */
@ViewScoped
@ManagedBean(name = "MbCargarMunicipios")
public class MbCargarMunicipios implements Serializable {

    @EJB
    private LogicaMunicipios logica;
    private boolean municipiosCreados;

    /**
     * Creates a new instance of MbCargarMunicipios
     */
    public MbCargarMunicipios() {
        municipiosCreados = false;
    }

    @PostConstruct
    public void init() {
        municipiosCreados = logica.municipiosCreados();
    }

    public String accionCargarMunicipios() {
        System.out.println("Cargando Municipios");
        FacesContext context = FacesContext.getCurrentInstance();
        FileInputStream file = null;
        int i = 0;
        try {
            String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("Dane2013.xlsx");
            file = new FileInputStream(new File(path));
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            logica.limpiarTablas();
            while (rowIterator.hasNext()) {
                i++;
                Row r = rowIterator.next();
                if (i == 1) {
                    continue;
                }
                String codigoDepartamento = r.getCell(0).getStringCellValue();
                String codigoMunicipio = r.getCell(1).getStringCellValue();
                String codigoCentroPoblado = r.getCell(2).getStringCellValue();
                String nombreDepartamento = r.getCell(3).getStringCellValue();
                String nombreMunicipio = r.getCell(4).getStringCellValue();
                String nombreCentroPoblado = r.getCell(5).getStringCellValue();
                String tipo = r.getCell(6).getStringCellValue();

                nombreCentroPoblado = quitarAcentos(nombreCentroPoblado);
                nombreDepartamento = quitarAcentos(nombreDepartamento);
                nombreMunicipio = quitarAcentos(nombreMunicipio);

                logica.registrarCentroPoblado(codigoDepartamento, codigoMunicipio,
                        codigoCentroPoblado, nombreDepartamento, nombreMunicipio,
                        nombreCentroPoblado, tipo);
                if (i % 100 == 0) {
                    System.out.println("Va por la fila: " + i);
                }
            }
            System.out.println("Filas: " + i);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Confirmacion", "Se han registrado los municipios"));
        } catch (FileNotFoundException ex) {
            System.err.println("FileNotFoundException en Fila: " + i);
            Logger.getLogger(MbCargarMunicipios.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.err.println("IOException en Fila: " + i);
            Logger.getLogger(MbCargarMunicipios.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            System.err.println("Exception en Fila: " + i);
            Logger.getLogger(MbCargarMunicipios.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (file != null) {
                    file.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(MbCargarMunicipios.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    private String quitarAcentos(String input) {
        // Cadena de caracteres original a sustituir.
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
        // Cadena de caracteres ASCII que reemplazarán los originales.
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        String output = input;
        for (int i = 0; i < original.length(); i++) {
            // Reemplazamos los caracteres especiales.
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }//for i
        return output;
    }

    public String accionTest() {
        System.out.println("probado");
        return null;
    }

    public boolean isMunicipiosCreados() {
        return municipiosCreados;
    }

    public void setMunicipiosCreados(boolean municipiosCreados) {
        this.municipiosCreados = municipiosCreados;
    }

}
