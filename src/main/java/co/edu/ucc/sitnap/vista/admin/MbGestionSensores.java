/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.vista.admin;

import co.edu.ucc.sitnap.modelo.base.Sensor;
import co.edu.ucc.sitnap.servicios.base.CommonsBean;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

/**
 *
 * @author Windows 8.1
 */
@ViewScoped
@ManagedBean(name = "MbGestionSensores")
public class MbGestionSensores implements Serializable {

    private Sensor sensor;
    private String latitud;
    private String longitud;
    private MapModel draggableModel;
    private Marker marker;

    public MbGestionSensores() {
    }

    @EJB
    private CommonsBean cb;

    @PostConstruct
    public void init() {
        sensor = new Sensor();
        draggableModel = new DefaultMapModel();

        //Shared coordinates
        LatLng coord1 = new LatLng(11.247141, -74.205504);

        //Draggable
        draggableModel.addOverlay(new Marker(coord1, "Sensor"));

        for (Marker premarker : draggableModel.getMarkers()) {
            premarker.setDraggable(true);
        }
    }

    public void onMarkerDrag(MarkerDragEvent event) {
        marker = event.getMarker();
        latitud = marker.getLatlng().getLat() + "";
        longitud = marker.getLatlng().getLng() + "";
    }

    public String accionGuarda() {
        sensor.setLatitud(latitud);
        sensor.setLongitud(longitud);
        if (cb.guardar(sensor)) {
            init();
        }
        return null;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public MapModel getDraggableModel() {
        return draggableModel;
    }

    public void setDraggableModel(MapModel draggableModel) {
        this.draggableModel = draggableModel;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

}
