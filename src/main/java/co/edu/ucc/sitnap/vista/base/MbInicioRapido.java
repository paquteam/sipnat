/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.vista.base;

import co.edu.ucc.sitnap.modelo.base.Usuario;
import co.edu.ucc.sitnap.seguridad.Md5;
import co.edu.ucc.sitnap.servicios.base.CommonsBean;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Windows 8.1
 */
@ViewScoped
@ManagedBean(name = "MbInicioRapido")
public class MbInicioRapido implements Serializable {

    private Usuario usuario;
    private String passwordVerificacion;

    @EJB
    private CommonsBean cb;

    public MbInicioRapido() {
    }

    @PostConstruct
    public void init() {
        usuario = new Usuario();
        List<Usuario> usuariosAdmin = cb.getByOneField(Usuario.class, "tipoUsuario", "ADMIN");
        if (!usuariosAdmin.isEmpty()) {
            redirect("/sitnap/index.xhtml");
        } 
    }

    public String accioGuarda() {
        if(verificarFormulario()){
            usuario.setTipoUsuario("ADMIN");
            usuario.setPassword(Md5.getEncoddedString(passwordVerificacion));
            if(cb.guardar(usuario)){
                mostrarMensaje(FacesMessage.SEVERITY_INFO, "Correcto", "Se ha guardado el administrador");
                init();
            }else{
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "No ha guardado el administrador");
            }
        }
        return null;
    }

    public Boolean verificarFormulario() {
        Boolean resultado = Boolean.TRUE;
        if (usuario.getCedula().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue nombre de usuario");
        }
        if (usuario.getNombreUsuario().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue nombre de usuario");
        }
        if (usuario.getApellidos().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue apellidos");
        }
        if (usuario.getNombres().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue nombres");
        }
        if (usuario.getPassword().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue password");
        }
        if (usuario.getEmail().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue correo electronico");
        }
        if (usuario.getTelefono().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue telefono");
        }
        if (passwordVerificacion.trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue password de verificacion");
        } else if (!usuario.getPassword().equals(passwordVerificacion)) {
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Los password no conincide");
            resultado = Boolean.FALSE;
        }
        return resultado;
    }

    public void mostrarMensaje(FacesMessage.Severity icono, String titulo, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(icono, titulo, mensaje));
    }

    private void redirect(String url) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            context.getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(MbInicioRapido.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getPasswordVerificacion() {
        return passwordVerificacion;
    }

    public void setPasswordVerificacion(String passwordVerificacion) {
        this.passwordVerificacion = passwordVerificacion;
    }
}
