/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.vista.admin;

import co.edu.ucc.sitnap.base.SessionOperations;
import co.edu.ucc.sitnap.modelo.base.Proyecto;
import co.edu.ucc.sitnap.modelo.base.Usuario;
import co.edu.ucc.sitnap.servicios.base.CommonsBean;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Windows 8.1
 */
@ViewScoped
@ManagedBean(name = "MbGestionProyecto")
public class MbGestionProyecto implements Serializable {

    private Usuario usuario;
    private Proyecto proyecto;
    private List<Proyecto> proyectos;
    private Boolean isPublico;

    @EJB
    private CommonsBean cb;

    public MbGestionProyecto() {
    }

    @PostConstruct
    public void init() {
        usuario = (Usuario) SessionOperations.getSessionValue("USUARIO");
        proyecto = new Proyecto();
        proyectos = cb.getByOneField(Proyecto.class, "usuarioCreacion", usuario.getNombreUsuario());
    }

    public String accionGuardar() {
        proyecto.setUsuarioDueño(usuario);
        if (cb.guardar(proyecto)) {
            mostrarMensaje(FacesMessage.SEVERITY_INFO, "Correcto", "Se ha guardado el proyecto");
            init();
        } else {
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "No ha guardado el proyecto");
        }
        return null;
    }
    
    public String accionEditar(Proyecto p){
        this.proyecto = p;
        return null;
    }

    public void mostrarMensaje(FacesMessage.Severity icono, String titulo, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(icono, titulo, mensaje));
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    public List<Proyecto> getProyectos() {
        return proyectos;
    }

    public void setProyectos(List<Proyecto> proyectos) {
        this.proyectos = proyectos;
    }

    public Boolean getIsPublico() {
        return isPublico;
    }

    public void setIsPublico(Boolean isPublico) {
        this.isPublico = isPublico;
    }
    
   
}
