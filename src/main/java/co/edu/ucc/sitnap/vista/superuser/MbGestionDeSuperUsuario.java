/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.vista.superuser;

import co.edu.ucc.sitnap.vista.admin.*;
import co.edu.ucc.sitnap.modelo.base.Usuario;
import co.edu.ucc.sitnap.seguridad.Md5;
import co.edu.ucc.sitnap.servicios.base.CommonsBean;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Windows 8.1
 */
@ViewScoped
@ManagedBean(name = "MbGestionDeSuperUsuario")
public class MbGestionDeSuperUsuario implements Serializable {

    private Usuario usuario;
    private List<Usuario> usuarios;

    private String passwordVerificacion;

    @EJB
    private CommonsBean cb;

    public MbGestionDeSuperUsuario() {
    }

    @PostConstruct
    public void init() {
        usuario = new Usuario();
        usuario.setEstado(Boolean.TRUE);
    }

    public String accionEditar(Usuario u) {
        this.usuario = u;
        return null;
    }

    public String accionGuardar() {
        if (verificarFormulario()) {
            usuario.setTipoUsuario("USER");
            usuario.setPassword(Md5.getEncoddedString(passwordVerificacion));
            if (cb.guardar(usuario)) {
                init();
            }
        }
        return null;
    }

    public Boolean verificarFormulario() {
        Boolean resultado = Boolean.TRUE;
        if (usuario.getCedula().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue nombre de usuario");
        }
        if (usuario.getNombreUsuario().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue nombre de usuario");
        }
        if (usuario.getApellidos().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue apellidos");
        }
        if (usuario.getNombres().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue nombres");
        }
        if (usuario.getPassword().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue password");
        }
        if (usuario.getEmail().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue correo electronico");
        }
        if (usuario.getTelefono().trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue telefono");
        }
        if (passwordVerificacion.trim().length() == 0) {
            resultado = Boolean.FALSE;
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Agregue password de verificacion");
        } else if (!usuario.getPassword().equals(passwordVerificacion)) {
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, "ERROR", "Los password no conincide");
            resultado = Boolean.FALSE;
        }
        return resultado;
    }

    public void mostrarMensaje(FacesMessage.Severity icono, String titulo, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(icono, titulo, mensaje));
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public String getPasswordVerificacion() {
        return passwordVerificacion;
    }

    public void setPasswordVerificacion(String passwordVerificacion) {
        this.passwordVerificacion = passwordVerificacion;
    }
}
