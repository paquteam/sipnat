/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.vista.base;

import co.edu.ucc.sitnap.base.SessionOperations;
import co.edu.ucc.sitnap.modelo.base.Usuario;
import co.edu.ucc.sitnap.servicios.base.CommonsBean;
import co.edu.ucc.sitnap.servicios.base.LogicaLogin;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Windows 8.1
 */
@SessionScoped
@ManagedBean(name = "MbLogin")
public class MbLogin implements Serializable {

    private Usuario usuario;
    private String nombreDeUsuaio;
    private String password;
    private boolean autenticado = false;
    private boolean usuarioAdmin = false;
    private boolean usuarioCliente = false;

    @EJB
    private CommonsBean cb;
    @EJB
    private LogicaLogin logica;
    
    /**
     * Creates a new instance of MbLogin
     */
    public MbLogin() {
    }

    @PostConstruct
    public void init() {
        usuario = new Usuario();
        nombreDeUsuaio = "";
        autenticado = false;
        usuarioAdmin = false;
        usuarioCliente = false;
        SessionOperations.setSessionValue("USUARIOCLIENTE", Boolean.FALSE);
        SessionOperations.setSessionValue("USUARIOADMIN", Boolean.FALSE);

    }

    public String accionLogin() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);
        autenticado = false;
        usuarioAdmin = false;
        usuarioCliente = false;
        SessionOperations.setSessionValue("USUARIOADMIN", Boolean.FALSE);
        SessionOperations.setSessionValue("USUARIOCLIENTE", Boolean.FALSE);

        Usuario uadmin = logica.loginAdmin(nombreDeUsuaio, password);
        if (uadmin != null) {
            usuario = uadmin;
            autenticado = true;
            usuarioAdmin = true;
            usuarioCliente = false;
            SessionOperations.setSessionValue("USUARIOADMIN", Boolean.TRUE);
            SessionOperations.setSessionValue("USUARIO", usuario);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "ADMINISTRADOR", "Bienvenido"));
            redirect("admin/index.xhtml");
        } else {
            System.out.println("no es admin");
            autenticado = false;
            usuarioAdmin = false;
            usuarioCliente = true;
            Usuario u = logica.loginCliente(nombreDeUsuaio, password);
            if (u != null) {
                System.out.println("usuario: " + u.getNombres());
                usuario = u;
                SessionOperations.setSessionValue("USUARIOCLIENTE", Boolean.TRUE);
                SessionOperations.setSessionValue("USUARIO", usuario);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido ", u.getNombres()));
                redirect("client/index.xhtml");
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Usuario y/o contraseÃ±as no validos"));
            }
        }
        return null;
    }
    
    public String accionLogout() {
        init();
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);
        try {
            SessionOperations.setSessionValue("USUARIOADMIN", Boolean.FALSE);
            SessionOperations.setSessionValue("USUARIOCLIENTE", Boolean.FALSE);
            context.getExternalContext().invalidateSession();
        } catch (Exception e) {

        }
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Salida", "Se ha cerrado la sesion correctamente"));
        String contextPath = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getContextPath();
        redirect(contextPath);

        return null;
    }


    private void redirect(String url) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            context.getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(MbLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombreDeUsuaio() {
        return nombreDeUsuaio;
    }

    public void setNombreDeUsuaio(String nombreDeUsuaio) {
        this.nombreDeUsuaio = nombreDeUsuaio;
    }

    public boolean isAutenticado() {
        return autenticado;
    }

    public void setAutenticado(boolean autenticado) {
        this.autenticado = autenticado;
    }

    public boolean isUsuarioAdmin() {
        return usuarioAdmin;
    }

    public void setUsuarioAdmin(boolean usuarioAdmin) {
        this.usuarioAdmin = usuarioAdmin;
    }

    public boolean isUsuarioCliente() {
        return usuarioCliente;
    }

    public void setUsuarioCliente(boolean usuarioCliente) {
        this.usuarioCliente = usuarioCliente;
    }    
}
