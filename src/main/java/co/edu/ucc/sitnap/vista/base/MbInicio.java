/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.vista.base;

import co.edu.ucc.sitnap.modelo.base.Proyecto;
import co.edu.ucc.sitnap.modelo.base.Usuario;
import co.edu.ucc.sitnap.servicios.base.CommonsBean;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author Windows 8.1
 */
@ViewScoped
@ManagedBean(name = "MbInicio")
public class MbInicio implements Serializable {

    private String string;

    private List<SelectItem> proyectoItems;
    private Long idProyecto;

    @EJB
    private CommonsBean cb;

    public MbInicio() {
    }

    @PostConstruct
    public void init() {
        List<Usuario> usuariosAdmin = cb.getByOneField(Usuario.class, "tipoUsuario", "ADMIN");
        if (usuariosAdmin.isEmpty()) {
            redirect("/sitnap/iniciorapido.xhtml");
        }
        proyectoItems = new LinkedList<>();
        List<Proyecto> proyectos = cb.getByOneField(Proyecto.class, "esPrivado", Boolean.FALSE);
        for (Proyecto p : proyectos) {
            proyectoItems.add(new SelectItem(p.getId(), p.getNombre() + " "  + p.getUsuarioDueño().getNombreUsuario()));
        }
        idProyecto = 0l;
    }

    private void redirect(String url) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            context.getExternalContext().redirect(url);
        } catch (IOException ex) {
            Logger.getLogger(MbInicioRapido.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public List<SelectItem> getProyectoItems() {
        return proyectoItems;
    }

    public void setProyectoItems(List<SelectItem> proyectoItems) {
        this.proyectoItems = proyectoItems;
    }

    public Long getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Long idProyecto) {
        this.idProyecto = idProyecto;
    }
    
    

}
