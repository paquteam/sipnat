/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.ucc.sitnap.modelo.base;

import co.edu.ucc.sitnap.modelo.CamposComunes;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Alvaro Padilla
 */
@Entity
public class Departamento extends CamposComunes implements Serializable {
    @OneToMany(mappedBy = "departamento")
    private List<Ciudad> ciudads;
    private static final long serialVersionUID = 1L;
    private String codigoDane;
    private String nombre;
    @ManyToOne
    private Pais pais;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre.toUpperCase();
    }

    public List<Ciudad> getCiudads() {
        return ciudads;
    }

    public void setCiudads(List<Ciudad> ciudads) {
        this.ciudads = ciudads;
    }

    public String getCodigoDane() {
        return codigoDane;
    }

    public void setCodigoDane(String codigoDane) {
        this.codigoDane = codigoDane;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    
}
