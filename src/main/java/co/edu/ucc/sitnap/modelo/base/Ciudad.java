/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.ucc.sitnap.modelo.base;

import co.edu.ucc.sitnap.modelo.CamposComunes;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Alvaro Padilla
 */
@Entity
public class Ciudad extends CamposComunes implements Serializable {
    private String codigoDane;
    private String nombre;
    @ManyToOne
    private Departamento departamento;
    @OneToMany(mappedBy = "ciudad")
    private List<CentroPoblado> centrodepoblados;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre.toUpperCase();
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public List<CentroPoblado> getCentrodepoblados() {
        return centrodepoblados;
    }

    public void setCentrodepoblados(List<CentroPoblado> centrodepoblados) {
        this.centrodepoblados = centrodepoblados;
    }

    public String getCodigoDane() {
        return codigoDane;
    }

    public void setCodigoDane(String codigoDane) {
        this.codigoDane = codigoDane;
    }

    
}
