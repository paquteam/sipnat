/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.modelo.base;

import co.edu.ucc.sitnap.modelo.CamposComunes;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Windows 8.1
 */
@Entity
public class Proyecto extends CamposComunes implements Serializable {

    @ManyToOne
    private Usuario usuarioDueño;
    @ManyToOne
    private Ciudad ciudad;
    private String nombre;
    private Boolean esPrivado;

    public Usuario getUsuarioDueño() {
        return usuarioDueño;
    }

    public void setUsuarioDueño(Usuario usuarioDueño) {
        this.usuarioDueño = usuarioDueño;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public Boolean getEsPrivado() {
        return esPrivado;
    }

    public void setEsPrivado(Boolean esPrivado) {
        this.esPrivado = esPrivado;
    }
}
