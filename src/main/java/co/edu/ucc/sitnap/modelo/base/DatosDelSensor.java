/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.modelo.base;

import co.edu.ucc.sitnap.modelo.CamposComunes;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Windows 8.1
 */
@Entity
public class DatosDelSensor extends CamposComunes implements Serializable {

    private Sensor sensor;
    private BigDecimal datosDelSensor;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechayhoraCreacion;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechayhoraDeCaptura;

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    public BigDecimal getDatosDelSensor() {
        return datosDelSensor;
    }

    public void setDatosDelSensor(BigDecimal datosDelSensor) {
        this.datosDelSensor = datosDelSensor;
    }

    public Date getFechayhoraCreacion() {
        return fechayhoraCreacion;
    }

    public void setFechayhoraCreacion(Date fechayhoraCreacion) {
        this.fechayhoraCreacion = fechayhoraCreacion;
    }

    public Date getFechayhoraDeCaptura() {
        return fechayhoraDeCaptura;
    }

    public void setFechayhoraDeCaptura(Date fechayhoraDeCaptura) {
        this.fechayhoraDeCaptura = fechayhoraDeCaptura;
    }
}
