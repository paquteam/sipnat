/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.edu.ucc.sitnap.modelo.base;


import co.edu.ucc.sitnap.modelo.CamposComunes;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author Alvaro Padilla
 */
@Entity
public class Barrio extends CamposComunes implements Serializable {
    private static final long serialVersionUID = 1L;
    private String nombre;
    private Double estrato;
    @ManyToOne
    private CentroPoblado centropoblado;
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre.toUpperCase();
    }

    public CentroPoblado getCentropoblado() {
        return centropoblado;
    }

    public void setCentropoblado(CentroPoblado centropoblado) {
        this.centropoblado = centropoblado;
    }

    public Double getEstrato() {
        return estrato;
    }

    public void setEstrato(Double estrato) {
        this.estrato = estrato;
    }
}
