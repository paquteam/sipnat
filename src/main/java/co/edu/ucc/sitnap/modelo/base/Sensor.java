/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.modelo.base;

import co.edu.ucc.sitnap.modelo.CamposComunes;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 *
 * @author Windows 8.1
 */
@Entity
public class Sensor extends CamposComunes implements Serializable {

    private String codigo;
    private String altitud;
    private String longitud;
    private String latitud;
    private String unidadDeMedida;
    private String estadoDelSensor;
    private BigDecimal limiteInferio;
    private BigDecimal limiteSuperior;
    @ManyToOne
    private Proyecto proyectPadre;

    @Transient
    private BigDecimal datoDelSensor;
    @Transient
    private Date fechayhoraDeCaptura;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getAltitud() {
        return altitud;
    }

    public void setAltitud(String altitud) {
        this.altitud = altitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public BigDecimal getLimiteInferio() {
        return limiteInferio;
    }

    public void setLimiteInferio(BigDecimal limiteInferio) {
        this.limiteInferio = limiteInferio;
    }

    public BigDecimal getLimiteSuperior() {
        return limiteSuperior;
    }

    public void setLimiteSuperior(BigDecimal limiteSuperior) {
        this.limiteSuperior = limiteSuperior;
    }

    public Proyecto getProyectPadre() {
        return proyectPadre;
    }

    public void setProyectPadre(Proyecto proyectPadre) {
        this.proyectPadre = proyectPadre;
    }

    public String getUnidadDeMedida() {
        return unidadDeMedida;
    }

    public void setUnidadDeMedida(String unidadDeMedida) {
        this.unidadDeMedida = unidadDeMedida;
    }

    public String getEstadoDelSensor() {
        return estadoDelSensor;
    }

    public void setEstadoDelSensor(String estadoDelSensor) {
        this.estadoDelSensor = estadoDelSensor;
    }

    public BigDecimal getDatoDelSensor() {
        return datoDelSensor;
    }

    public void setDatoDelSensor(BigDecimal datoDelSensor) {
        this.datoDelSensor = datoDelSensor;
    }

    public Date getFechayhoraDeCaptura() {
        return fechayhoraDeCaptura;
    }

    public void setFechayhoraDeCaptura(Date fechayhoraDeCaptura) {
        this.fechayhoraDeCaptura = fechayhoraDeCaptura;
    }
}
