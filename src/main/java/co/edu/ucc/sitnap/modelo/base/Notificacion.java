/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.modelo.base;

import co.edu.ucc.sitnap.modelo.CamposComunes;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Windows 8.1
 */
@Entity
public class Notificacion extends CamposComunes implements Serializable {
    private String asunto;
    @Column(columnDefinition = "TEXT")
    private String mensaje;
    @ManyToOne
    private Usuario usuarioRemitente;
    @ManyToOne
    private Usuario usuarioDestino;
    private Boolean estaLeido;

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Usuario getUsuarioRemitente() {
        return usuarioRemitente;
    }

    public void setUsuarioRemitente(Usuario usuarioRemitente) {
        this.usuarioRemitente = usuarioRemitente;
    }

    public Usuario getUsuarioDestino() {
        return usuarioDestino;
    }

    public void setUsuarioDestino(Usuario usuarioDestino) {
        this.usuarioDestino = usuarioDestino;
    }

    public Boolean getEstaLeido() {
        return estaLeido;
    }

    public void setEstaLeido(Boolean estaLeido) {
        this.estaLeido = estaLeido;
    }
    
    
}
