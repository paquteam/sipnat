/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucc.sitnap.ws;

import co.edu.ucc.sitnap.modelo.base.DatosDelSensor;
import co.edu.ucc.sitnap.modelo.base.Sensor;
import co.edu.ucc.sitnap.servicios.base.CommonsBean;
import com.google.gson.Gson;
import java.util.Date;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Windows 8.1
 */
@Path("sensor")
@RequestScoped
public class SensorResource {

    @EJB
    private CommonsBean cb;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of SensorResource
     */
    public SensorResource() {
    }

    /**
     * Retrieves representation of an instance of
     * co.edu.ucc.sitnap.ws.SensorResource
     *
     * @param sensor
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    @Path("/{sensor}")
    public String getJson(@PathParam("sensor") String sensor) throws Exception {
        Gson gson = new Gson();
        Sensor s = new Sensor();
        s = gson.fromJson(sensor, Sensor.class);
        Sensor sensorDeVerificacion = (Sensor) cb.getByOneFieldWithOneResult(Sensor.class, "codigo", s.getCodigo());
        if(sensorDeVerificacion == null){
            return "001";
        }else{
            sensorDeVerificacion.setEstadoDelSensor("De alta");
            if(cb.guardar(sensorDeVerificacion)){
                return "003";
            }else{
                return "002";
            }
        }
    }

    /**
     * PUT method for updating or creating an instance of SensorResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Produces(MediaType.TEXT_PLAIN)
    public String putJson(String content) throws Exception {
        Gson gson = new Gson();
        Sensor s = new Sensor();
        s = gson.fromJson(content, Sensor.class);
        Sensor sensorDeVerificacion = (Sensor) cb.getByOneFieldWithOneResult(Sensor.class, "codigo", s.getCodigo());
        if (sensorDeVerificacion != null) {
            if (!sensorDeVerificacion.getEstadoDelSensor().equals("Conectado")) {
                sensorDeVerificacion.setEstadoDelSensor("Conectado");
                if (cb.guardar(sensorDeVerificacion)) {
                    DatosDelSensor dds = new DatosDelSensor();
                    dds.setSensor(sensorDeVerificacion);
                    dds.setFechayhoraCreacion(new Date());
                    dds.setDatosDelSensor(s.getDatoDelSensor());
                    dds.setFechayhoraDeCaptura(s.getFechayhoraDeCaptura());
                    if (cb.guardar(dds)) {
                        return "003";//Correcto
                    } else {
                        return "004";//Error al registra el dato
                    }

                } else {
                    return "002";//Error al modificar el estado del  sensor 
                }
            } else {
                DatosDelSensor dds = new DatosDelSensor();
                dds.setSensor(sensorDeVerificacion);
                dds.setFechayhoraCreacion(new Date());
                dds.setDatosDelSensor(s.getDatoDelSensor());
                dds.setFechayhoraDeCaptura(s.getFechayhoraDeCaptura());
                if (cb.guardar(dds)) {
                    return "003";//Correcto
                } else {
                    return "004";//Error al registra el dato
                }
            }
        } else {
            return "001";//Sensor no existe
        }
    }
}
